import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<StatefulWidget> {
  //locate the user position
  late LatLng userPosition;
  List<Marker> markers = [];
 
 @override
  void setState(VoidCallback fn) {
    // TODO: implement setState
    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('HomeScreen'),
        ),
        body: FutureBuilder(
          //hàm trả về vị trí hiện tại ko trả về ngay => hàm bất đồng bộ
          //phụ thuộc vào mạng, phân quyền, tốc độ phản hồi của google map
          future: findUserLocation(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return GoogleMap(
                initialCameraPosition:
                    CameraPosition(target: snapshot.data, zoom: 12),
                markers: Set<Marker>.of(markers),
              );
            } else {
              return _setLoading();
            }
          },
        ));
  }

  Widget _setLoading() {
    return Scaffold(
      body: _setContentLoading(),
    );
  }

//màn hình chờ
  Widget _setContentLoading() {
    return Align(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const <Widget>[
          Text(
            'Please wait',
            style: TextStyle(fontSize: 25, color: Colors.blueAccent),
          ),
          //khoảng trống
          SizedBox(
            height: 20,
          ),
          //icon loading
          CircularProgressIndicator(),
        ],
      ),
    );
  }

  Future<LatLng> findUserLocation() async {
    //lấy location
    Location location = Location();
    LocationData userLocation;
    //quyền
    PermissionStatus hasPermission = await location.hasPermission();
    //người dùng cho phép thì active
    bool active = await location.serviceEnabled();
    // userLocation = await location.getLocation();
    // userPosition = LatLng(
    //       userLocation.latitude as double, userLocation.longitude as double);
    return userPosition;
    //cho phép thì gửi vị trí hiện tại
    if (hasPermission == PermissionStatus.granted && active) {
      userLocation = await location.getLocation();
      userPosition = LatLng(
          userLocation.latitude as double, userLocation.longitude as double);
    } // không cho thì gửi vị trí mặc định
    else {
      print('Use default location: ');
      //TDT uni
      userPosition = LatLng(10.732869174213993, 106.69973741130023);
    }
    //Display marker at the user position
    if (markers.isEmpty) {
      markers.add(buildMaker(userPosition));
    } else {
      markers[0] = buildMaker(userPosition);
    }

    setState(() {});

    return userPosition;
  }

  Marker buildMaker(LatLng userPosition) {
    MarkerId markerId = MarkerId('H');
    Marker marker = Marker(markerId: markerId, position: userPosition);
    return marker;
  }
}
